#include <iostream>
#include <map>

using namespace std;

void PrintMap(const map<char, int>& theMap){
    map<char, int>::const_iterator record = theMap.begin();
    while (record != theMap.end()) {
        cout << record->first << " " << record->second << endl;
        record++;
    }

}

void StringToMap(map<char, int>& contentsOfString, const string& string){
    for(char c : string){
        if(c != ' '){
            auto it = contentsOfString.find(c);
            if(it != contentsOfString.end())
                it->second++;
            else
                contentsOfString[c] = 1;
        }
    }
}

bool IsAnagram(const string& string1, const string& string2){
    map<char, int> contentsOfString1;
    map<char, int> contentsOfString2;
    StringToMap(contentsOfString1, string1);
    StringToMap(contentsOfString2, string2);
//    PrintMap(contentsOfString1);
//    PrintMap(contentsOfString2);
    return contentsOfString1 == contentsOfString2;
}

int main()
{
    string string1 = "лекарство";
    string string2 = "стекловар";
    cout << (IsAnagram(string1, string2) ? "Anagram." : "Not anagram.") << endl;
    return 0;
}
