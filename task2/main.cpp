#include <iostream>
#include <map>

using namespace std;

int main()
{
    map<string, int> registryList;
    string inputString;

    while(1){
        cout << "Enter a command (name, next, quit): " ;
        cin >> inputString;
        if(inputString == "quit")
            break;
        else if(inputString == "next"){
            if(registryList.size() == 0)
                cout << "There are no people in registry." << endl;
            else{
                cout << registryList.begin()->first << endl;
                if(registryList.begin()->second > 1)
                    registryList.begin()->second--;
                else
                    registryList.erase(registryList.begin());
            }
        }
        else{
            auto it = registryList.find(inputString);
            if(it != registryList.end())
                it->second++;
            else
                registryList[inputString] = 1;
        }
    }
    return 0;
}
