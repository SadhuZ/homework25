#include <iostream>
#include <map>
using namespace std;

void PrintPhoneBook(map<string, string>& phoneBook){
    map<string, string>::iterator record = phoneBook.begin();
    while (record != phoneBook.end()) {
        cout << record->first << " " << record->second << endl;
        record++;
    }

}

bool IsPhoneNumber(const string& phoneNumber){
    if(phoneNumber.size() != 8)
        return false;
    if(phoneNumber[2] != '-' || phoneNumber[5] != '-')
        return false;
    if((phoneNumber[0] < '0' || phoneNumber[0] > '9') ||
        (phoneNumber[1] < '0' || phoneNumber[1] > '9') ||
        (phoneNumber[3] < '0' || phoneNumber[3] > '9') ||
        (phoneNumber[4] < '0' || phoneNumber[4] > '9') ||
        (phoneNumber[6] < '0' || phoneNumber[6] > '9') ||
        (phoneNumber[7] < '0' || phoneNumber[7] > '9'))
        return false;
    return true;
}

void AddRecord(map<string, string>& phoneBook){
    string phoneNumber;
    string name;
    cout << "Enter a phone number: " << endl;
    cin >> phoneNumber;
    while(!IsPhoneNumber(phoneNumber)){
        cout << "Wrong phone number!" << endl;
        cout << "Enter a phone number: " << endl;
        cin >> phoneNumber;
    }
    cout << "Enter a name: " << endl;
    cin >> name;

    char answer;
    if(phoneBook.find(phoneNumber) != phoneBook.end()){
        cout << "The phone number already exists. Replace the name? (y/n): ";
        cin >> answer;
        if(answer == 'y')
            phoneBook[phoneNumber] = name;
        else
            phoneBook.insert(make_pair(phoneNumber, name));
    }
    else
        phoneBook.insert(make_pair(phoneNumber, name));
}

void SearchByNumber(const map<string, string>& phoneBook){
    string phoneNumber;
    cout << "Enter the phone number for search: " << endl;
    cin >> phoneNumber;
    while(!IsPhoneNumber(phoneNumber)){
        cout << "Wrong phone number!" << endl;
        cout << "Enter a phone number: " << endl;
        cin >> phoneNumber;
    }
    map<string, string>::const_iterator record = phoneBook.find(phoneNumber);
    cout << record->first << " " << record->second << endl;
}

void SearchByName(const map<string, string>& phoneBook){
    string name;
    cout << "Enter the name for search: " << endl;
    cin >> name;
    for(auto record : phoneBook){
        if(record.second == name)
            cout << record.first << " " << record.second << endl;
    }
}

int main()
{
    int command;
    map<string, string> phoneBook;

    while(1){
        cout << "Enter a command (1 - add a record, 2 - search by number, 3 - search by name, 0 - quit): " << endl;
        cin >> command;
        if(command == 0) break;
        switch (command) {
            case 1:
                AddRecord(phoneBook);
                PrintPhoneBook(phoneBook);
            break;
            case 2:
                SearchByNumber(phoneBook);
            break;
            case 3:
                SearchByName(phoneBook);
            break;
            default:
                cout << "Wrong command!" << endl;;
            break;
        }
    }
    return 0;
}
